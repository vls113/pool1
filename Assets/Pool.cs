﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolObject 
{ 
    public GameObject prefab;
    public int amout;
}

public class Pool : MonoBehaviour
{
    [SerializeField] private Transform spawner;

    public List<PoolObject> pool = new List<PoolObject>();
    public List<GameObject> itemsOnScene = new List<GameObject>();

    private void Start()
    {
        foreach (PoolObject item in pool) 
        {
            for (int i = 0; i < item.amout; i++) 
            {
                GameObject obj = Instantiate(item.prefab, spawner);
                itemsOnScene.Add(obj);
                obj.SetActive(false);
            }
        }
    }

    public GameObject Get(string tag) 
    {
        //пройтись по всему пулу, найти объекс с совпадающим тегом, проверить, включен ли он
        //если выключен - возвращаем
        //если включен - возвращаем null
        return null;
    }
}
